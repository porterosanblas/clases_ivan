package dao;


import objects.Cliente;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.IllegalFormatCodePointException;

public class DaoClientes {

    private static ArrayList<Cliente> clientes = new ArrayList<>();
    Cliente cliente1 = new Cliente("Alvaro", "Fernandez", "Lopez", LocalDate.of(1999,01,11), "1234567F");
    Cliente cliente2 = new Cliente("Alvaro", "Fernandez", "Lopez", LocalDate.of(1999,01,11), "1234567F");
    public void init(){
        clientes.add(cliente1);
        clientes.add(cliente2);

    }

    public boolean darDeAlta(Cliente cliente){

        int longitudAnterior = getAllClientes().size();
        int longitudPosterior = 0;

        clientes.add(cliente);

        longitudPosterior = clientes.size();

        if (longitudAnterior<longitudPosterior){
            return true;
        }else{
            return false;
        }

    }

    public ArrayList<Cliente> getAllClientes(){
        return clientes;
    }
}
