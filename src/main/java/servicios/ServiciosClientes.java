package servicios;

import dao.DaoClientes;
import objects.Cliente;

import java.util.ArrayList;

public class ServiciosClientes {

    DaoClientes daoClientes = new DaoClientes();

    public void init(){
       daoClientes.init();
    }

    public ArrayList<Cliente> getAllClientes(){
        return daoClientes.getAllClientes();
    }




    public boolean darDeAlta(Cliente cliente){
        return daoClientes.darDeAlta(cliente);
    }
}
