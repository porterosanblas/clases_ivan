package org.example;

import objects.Cliente;
import servicios.ServiciosClientes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        ServiciosClientes serviciosClientes = new ServiciosClientes();
        serviciosClientes.init();
        Scanner sc = new Scanner(System.in);
        Cliente cliente = new Cliente();
        ArrayList<Cliente> clientes = new ArrayList<>();

        clientes = serviciosClientes.getAllClientes();


        for (Cliente cliente1: clientes) {
            System.out.println(cliente1);
        }

        System.out.println("Introduce tu nombre");
        cliente.setNombre(sc.nextLine());
        System.out.println("Introduce tu primer apellido");
        cliente.setApellido1(sc.nextLine());
        System.out.println("Introduce tu segundo apellido");
        cliente.setApellido2(sc.nextLine());
        //System.out.println("Introduce tu fecha de nacimiento");
        cliente.setFechaNacimiento(LocalDate.now());
        System.out.println("Introduce tu DNI");
        cliente.setDni(sc.nextLine());

        boolean added = serviciosClientes.darDeAlta(cliente);

        if (added){
            System.out.println("Cliente dado de alta correctamente");
        }else{
            System.out.println("Error 404");
        }

        //clientes.clear();

        clientes = serviciosClientes.getAllClientes();

        for (Cliente cliente1: clientes) {
            System.out.println(cliente1);
        }

        System.out.println("Hola que tal");



    }
}